#include "../Library/ImageBase.h"
#include "../Library/AES.h"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int key; vector<unsigned char> keys; const unsigned char* getKeys(){return keys.data();}

void initKey(){ cout << "Initialising key " << key << endl;	srand (key);}

int nextKey(int mod = 256){rand()%mod;}

void loadKey(int count,int mod = 256)
{
	for(int i=0; i < count;i++)
	{
		keys.push_back(nextKey(mod));
	}
}

enum class METHOD{AES_BLOC,CBC_BLOC,CFB_BLOC};
string METHODS[3] = {"AES_BLOC","CBC_BLOC","CFB_BLOC"};

ostream& operator<<(ostream& os, const METHOD& m);
ostream& operator<<(ostream& os, const METHOD& m)
{
    os << METHODS[(int)m];
    return os;
}

float getRand(){return rand()/(float)RAND_MAX;}

AES aes;


unsigned char* aes_bloc_chiffre(const unsigned char* dataIn,int size,const unsigned char* keys)
{
	unsigned char* dataOut = aes.EncryptECB(dataIn,size,keys);

	return dataOut;
}

unsigned char* aes_bloc_dechiffre(const unsigned char* dataIn,int size,const unsigned char* keys)
{
	unsigned char* dataOut = aes.DecryptECB(dataIn,size,keys);

	return dataOut;
}

unsigned char* cbc_bloc_chiffre(const unsigned char* dataIn,int size,const unsigned char* keys)
{
	unsigned char* iv = new unsigned char[256];
	for(int i = 0; i < 256; ++i){iv[i] = 0;}

	unsigned char* dataOut = aes.EncryptCBC(dataIn,size,keys,iv);

	return dataOut;
}

unsigned char* cbc_bloc_dechiffre(const unsigned char* dataIn,int size,const unsigned char* keys)
{
	unsigned char* iv = new unsigned char[256];
	for(int i = 0; i < 256; ++i){iv[i] = 0;}
	unsigned char* dataOut = aes.DecryptCBC(dataIn,size,keys,iv);

	return dataOut;
}


unsigned char* cfb_bloc_chiffre(const unsigned char* dataIn,int size,const unsigned char* keys)
{
	unsigned char* iv = new unsigned char[256];
	for(int i = 0; i < 256; ++i){iv[i] = 0;}

	unsigned char* dataOut = aes.EncryptCFB(dataIn,size,keys,iv);

	return dataOut;
}

unsigned char* cfb_bloc_dechiffre(const unsigned char* dataIn,int size,const unsigned char* keys)
{
	unsigned char* iv = new unsigned char[256];
	for(int i = 0; i < 256; ++i){iv[i] = 0;}
	unsigned char* dataOut = aes.DecryptCFB(dataIn,size,keys,iv);

	return dataOut;
}
ImageBase* chiffre(ImageBase* imageIn, METHOD method)
{
	ImageBase* imgOut = new ImageBase(imageIn->getWidth(), imageIn->getHeight(),false);

	loadKey(imgOut->getSize());	

	unsigned char* dataIn = imageIn->getData();
	unsigned char* dataOut;

	switch(method)
	{
		case METHOD::AES_BLOC:
			dataOut = aes_bloc_chiffre(dataIn,imgOut->getSize(),getKeys());
			break;
		case METHOD::CBC_BLOC:
			dataOut = cbc_bloc_chiffre(dataIn,imgOut->getSize(),getKeys());
			break;
		case METHOD::CFB_BLOC:
			dataOut = cfb_bloc_chiffre(dataIn,imgOut->getSize(),getKeys());
			break;
	}

	imgOut->setData(dataOut);

	return imgOut;
}

ImageBase* dechiffre(ImageBase* imageIn, METHOD method)
{
	ImageBase* imgOut = new ImageBase(imageIn->getWidth(), imageIn->getHeight(),false);

	loadKey(imgOut->getSize());	

	unsigned char* dataIn = imageIn->getData();
	unsigned char* dataOut;

	switch(method)
	{
		case METHOD::AES_BLOC:
			dataOut = aes_bloc_dechiffre(dataIn,imgOut->getSize(),getKeys());
			break;
		case METHOD::CBC_BLOC:
			dataOut = cbc_bloc_dechiffre(dataIn,imgOut->getSize(),getKeys());
			break;
		case METHOD::CFB_BLOC:
			dataOut = cfb_bloc_dechiffre(dataIn,imgOut->getSize(),getKeys());
			break;
	}

	imgOut->setData(dataOut);

	return imgOut;
}

void addNoise(ImageBase* imageIn)
{
	for(int x = 0 ; x < imageIn->getWidth();x += 16)
	{
		for(int y = 0 ; y < imageIn->getHeight();y += 16)
		{
			int dx = (float)(getRand() * 16.0);
			int dy = (float)(getRand() * 16.0);
			float r = getRand();
			imageIn->set(x,y,0,imageIn->get(x+dx,y+dy,0) + (-1 + (2*r)) );
		}
	}
}


int main(int argc, char **argv)
{
	if (argc != 6)
	{
		printf("Usage: (Permut/Subs/Crack) ImageIn ; ImageOut ; Key ; Dechiffrer? \n");
		return 1;
	}
	char imageInPath[250] , imageOutPath[250], imageDechiffrePath[250]; int direction; int actionId; METHOD action;
	sscanf (argv[1],"%d",&actionId) ;
	sscanf (argv[2],"%s",imageInPath) ;
	sscanf (argv[3],"%s",imageOutPath) ;
	sscanf (argv[4],"%d",&key) ;
	sscanf (argv[5],"%d",&direction) ;

	initKey();
	action = (METHOD)actionId;
	aes = AES();

	cout << endl;

	cout << "Loading image " << imageInPath << endl;

	ImageBase* image = new ImageBase(); image->load(imageInPath);
	cout << " > Size : " << image->getSize() << " | " << (image->getColor() ? "PPM" : "PGM") << endl;
	
	cout << "Entropy : " << image->entropy(0) << endl;	
	image->saveHistogram(image->histogram(0),"histoIn.dat")
	;

	ImageBase* imgOut;

	if(direction == 0)
	{
		cout << "Chiffrement with " << action << endl;
		imgOut = chiffre(image,action);
		addNoise(imgOut);
	}
	else
	{
		cout << "Dechiffrement with " << action << endl;
		imgOut = dechiffre(image,action);
	}

	cout << "Entropy : " << imgOut->entropy(0) << endl;	
	cout << "PSNR : " << imgOut->PSNR(image) << endl;	
	imgOut->saveHistogram(imgOut->histogram(0),"histoOut.dat");
	cout << "Saving image "<< endl;
	imgOut->save(imageOutPath);
	

	return 0;
}


#include "../Library/ImageBase.h"
#include "../Library/AES.h"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int key; vector<unsigned char> keys; const unsigned char* getKeys(){return keys.data();}

void initKey(){ cout << "Initialising key " << key << endl;	srand (key);}

int nextKey(int mod = 256){rand()%mod;}

void loadKey(int count,int mod = 256)
{
	for(int i=0; i < count;i++)
	{
		keys.push_back(nextKey(mod));
	}
}

float getRand(){return rand()/(float)RAND_MAX;}

AES aes;


unsigned char* aes_bloc_chiffre(const unsigned char* dataIn,int size,const unsigned char* keys)
{
	unsigned char* dataOut = aes.EncryptECB(dataIn,size,keys);

	return dataOut;
}

unsigned char* aes_bloc_dechiffre(const unsigned char* dataIn,int size,const unsigned char* keys)
{
	unsigned char* dataOut = aes.DecryptECB(dataIn,size,keys);

	return dataOut;
}

unsigned char* chiffre(unsigned char* dataIn, int size)
{
	unsigned char* dataOut = aes_bloc_chiffre(dataIn,size,getKeys());

	return dataOut;
}

unsigned char* dechiffre(unsigned char* dataIn, int size)
{
	unsigned char* dataOut = aes_bloc_dechiffre(dataIn,size,getKeys());
			
	return dataOut;
}

string readFile(const char* path)
{
	fstream newfile; string tp;
	newfile.open(path,ios::in); 
   if (newfile.is_open())
   {
   	string str = "";
      while(getline(newfile, str))
      { 
      	tp += str;
         //cout << tp << "\n"; 
      }
      newfile.close(); 
   }
   return tp;
}

void convertToImageSize(string & dataIn,int size)
{
	while(dataIn.size() < size)
	{
		dataIn += ".";
	}
}

unsigned char* charToUChar(const char* dataIn,int size)
{
	unsigned char* dataOut = new unsigned char[size];

	for(int i = 0; i < size; ++i)
	{
		dataOut[i] = (unsigned char)dataIn[i];
	}

	return dataOut;
}
char* uCharToChar(unsigned char* dataIn,int size)
{
	char* dataOut = new char[size];

	for(int i = 0; i < size; ++i)
	{
		dataOut[i] = (char)dataIn[i];
	}

	return dataOut;
}

string charToString(const char* data, int size)
{
	string str = "";

	for(int i = 0; i < size; ++i)
	{
		str += data[i];
	}

	return str;
}

void writeFile(const char* path, string text)
{
	fstream newfile; string tp;
	newfile.open(path,ios::out); 
   if (newfile.is_open())
   {
      newfile << text;
      newfile.close(); 
   }
}

bool getBit(unsigned char value, int k)
{
	unsigned char mask = pow(2,k);
	return (value & mask) > 1;
}

bool* binaryPlane(ImageBase* image, int k)
{
	bool* data = new bool[image->getSize()];

	unsigned char mask = pow(2,k);

	for(int i = 0; i < image->getSize(); i++)
	{
		int value = image->get(i,0) & mask;
		data[i] = value > 0;
	}

	return data;
}

vector<bool*> extractPlanes(ImageBase* image)
{
	vector<bool*> list;

	for(int k = 0; k < 8; k++)
	{
		list.push_back(binaryPlane(image,k));
	}

	return list;
}

ImageBase* combineBinaryPlanes(vector<bool*> planes, int width, int heigth)
{
	ImageBase* image = new ImageBase(width,heigth,false);

	for(int k = 0; k < 8; k++)
	{
		for(int i = 0; i < image->getSize();i++)
		{
			int value = pow(2,k) * (planes.at(k)[i] ? 1 : 0);
			image->set(i,0,image->get(i,0) + value);
		}
	}

	return image;
}

ImageBase* binaryToPGM(bool* data, int width, int heigth)
{
	ImageBase* image = new ImageBase(width,heigth,false);

	for(int i = 0; i < image->getSize(); i++)
	{
		image->set(i,0, data[i] ? 255 : 0);
	}

	return image;
}

void insertInPlane(bool* plane,unsigned char* data, int size)
{
	for(int i = 0; i < size; i++)
	{
		int k = i % 8;
		int j = i / 8;
		bool bit = getBit(data[j],k);
		plane[i] = bit;
	}
}

unsigned char* extractFromPlane(bool* plane, int size)
{
	unsigned char* data = new unsigned char[size/8];

	for(int i = 0; i < size/8; i++){data[i] = 0;}

	for(int i = 0; i < size; i++)
	{
		int j = i/8;
		int k = i%8;
		data[j] += pow(2,k) * (plane[i] ? 1 : 0);
	}

	return data;
}

int main(int argc, char **argv)
{
	if (argc != 7)
	{
		printf("Usage:ImageIn ; ImageOut ; message.txt ; key ; bit number; Dechiffrer? \n");
		return 1;
	}
	char imageInPath[250] , imageOutPath[250], messagePath[250]; int direction; int k;
	sscanf (argv[1],"%s",imageInPath) ;
	sscanf (argv[2],"%s",imageOutPath) ;
	sscanf (argv[3],"%s",messagePath) ;
	sscanf (argv[4],"%d",&key) ;
	sscanf (argv[5],"%d",&k) ;
	sscanf (argv[6],"%d",&direction) ;

	initKey();	
	aes = AES();	


	cout << endl;

	cout << "Loading image " << imageInPath << endl;

	ImageBase* image = new ImageBase(); image->load(imageInPath);
	cout << " > Size : " << image->getSize() << " | " << (image->getColor() ? "PPM" : "PGM") << endl;
	
	cout << "Entropy : " << image->entropy(0) << endl;	
	image->saveHistogram(image->histogram(0),"histoIn.dat");



	//cout << "Message = " << message  << " size : " << message.size() << endl;

	ImageBase* imgOut;

	if(direction == 0)
	{		
		cout << "Reading message text" << endl;
		string message = readFile(messagePath);
		convertToImageSize(message,image->getSize()/8);
		
		unsigned char* messageData = charToUChar(message.c_str(),message.size());

		
		loadKey(message.size());
		//unsigned char* dataChiffre = chiffre( messageData,message.size());

		vector<bool*> planes = extractPlanes(image);

		insertInPlane(planes[k],messageData,image->getSize());

		imgOut = combineBinaryPlanes(planes,image->getWidth(),image->getHeight());
		
		/*planes = extractPlanes(imgOut);
		
		unsigned char* dataChiffre = extractFromPlane(planes.at(k),image->getSize());

		string messageDechiffre = charToString(uCharToChar(dataChiffre,image->getSize()/8),image->getSize()/8);
		
		cout << messageDechiffre << endl;*/			

		cout << "Entropy : " << imgOut->entropy(0) << endl;	
		cout << "PSNR : " << imgOut->PSNR(image) << endl;	
		imgOut->saveHistogram(imgOut->histogram(0),"histoOut.dat");
		cout << "Saving image "<< endl;
		imgOut->save(imageOutPath);
	}
	else
	{
		vector<bool*> planes = extractPlanes(image);
		unsigned char* dataChiffre = extractFromPlane(planes.at(k),image->getSize());

		loadKey(image->getSize());
		//unsigned char* dataDechiffre =  dechiffre(dataChiffre,image->getSize());

		string messageDechiffre = charToString(uCharToChar(dataChiffre,image->getSize()/8),image->getSize()/8);

		cout << messageDechiffre << endl;

		//cout << "Message dechiffre = " << messageDechiffre << " size : " << messageDechiffre.size() << endl;
	 
		cout << "Writing message back" << endl;
		writeFile(messagePath,messageDechiffre);
	}


	

	

	return 0;
}


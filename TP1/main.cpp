#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

ImageBase* permutation(ImageBase* image)
{
	ImageBase* imgOut = new ImageBase(image->getWidth(), image->getHeight(),false);
	
	vector<int> positions;
	for(int i = 0; i < imgOut->getSize();i++){positions.push_back(i);}

	for(int i = 0; i < imgOut->getSize(); i++)
	{
		int pos = rand() % positions.size();
		
		imgOut->set(positions.at(pos),0,image->get(i,0));
		
		positions.erase(positions.begin() + pos);
	}
	
	return imgOut;
}


ImageBase* depermutation(ImageBase* image)
{
	ImageBase* imgOut = new ImageBase(image->getWidth(), image->getHeight(),false);
	
	vector<int> positions;
	for(int i = 0; i < imgOut->getSize();i++){positions.push_back(i);}

	for(int i = 0; i < imgOut->getSize(); i++)
	{
		int pos = rand() % positions.size();
		
		imgOut->set(i,0,image->get(positions.at(pos),0));
		
		positions.erase(positions.begin() + pos);
	}

	return imgOut;
}

ImageBase* subtitution(ImageBase* image)
{
	ImageBase* imgOut = new ImageBase(image->getWidth(), image->getHeight(),false);

	imgOut->set(0,0,(image->get(0,0) + (rand()%256)));

	for(int i = 1; i < imgOut->getSize(); i++)
	{
		int val = (imgOut->get(i-1,0) + image->get(i,0) + (rand() % 256)) % 256;
		
		imgOut->set(i,0,val);
	}
	
	return imgOut;
}

ImageBase* desubtitution(ImageBase* image,int key)
{	
	srand(key);
	
	ImageBase* imgOut = new ImageBase(image->getWidth(), image->getHeight(),false);
	
	vector<int> rands;
	for(int i = 0; i < imgOut->getSize();i++){rands.push_back(rand()%256);}
	

	for(int i = imgOut->getSize() - 1; i > 0; i--)
	{
		int val = (image->get(i,0) - image->get(i-1,0) - rands.at(i)) % 256;
		
		imgOut->set(i,0,val);
	}
	
	imgOut->set(0,0,(image->get(0,0) - rands.at(0))%256);
	
	return imgOut;
}

int main(int argc, char **argv)
{
	if (argc != 6)
	{
		printf("Usage: (Permut/Subs/Crack) ImageIn ; ImageOut ; Key ; Dechiffrer? \n");
		return 1;
	}
	char imageInPath[250] , imageOutPath[250], imageDechiffrePath[250]; int key; int direction; int action;
	sscanf (argv[1],"%d",&action) ;
	sscanf (argv[2],"%s",imageInPath) ;
	sscanf (argv[3],"%s",imageOutPath) ;
	sscanf (argv[4],"%d",&key) ;
	sscanf (argv[5],"%d",&direction) ;
	
	srand (key);


	cout << endl;

	cout << "Loading image " << imageInPath << endl;

	ImageBase* image = new ImageBase(); image->load(imageInPath);
	cout << " > Size : " << image->getSize() << " | " << (image->getColor() ? "PPM" : "PGM") << endl;
	
	cout << "Entropy : " << image->entropy(0) << endl;
	
	image->saveHistogram(image->histogram(0),"histoIn.dat");
	
	if(action == 0)
	{
		if(direction == 0)
		{
			cout << "Permutation with key " << key << endl;
			ImageBase* imgOut = permutation(image); // new ImageBase(image->getWidth(), image->getHeight(),false);

			double psnr = image->PSNR(imgOut);
			cout << "PSNR : " << psnr << endl;
			
			cout << "Entropy : " << imgOut->entropy(0) << endl;
			
			imgOut->saveHistogram(imgOut->histogram(0),"histoOut.dat");
			
			cout << "Saving chifree" << endl;
			imgOut->save(imageOutPath);
		}
		else
		{	
			cout << "Depermutation with key " << key << endl;
			ImageBase* imgOut = depermutation(image); // new ImageBase(image->getWidth(), image->getHeight(),false);

			double psnr = image->PSNR(imgOut);
			cout << "PSNR : " << psnr << endl;
			
			cout << "Entropy : " << imgOut->entropy(0) << endl;
			
			imgOut->saveHistogram(imgOut->histogram(0),"histoOut.dat");
			
			cout << "Saving dechifree" << endl;
			imgOut->save(imageOutPath);
		}
	}
	else if(action == 1)
	{
		if(direction == 0)
		{
			cout << "Substitution with key " << key << endl;
			ImageBase* imgOut = subtitution(image); // new ImageBase(image->getWidth(), image->getHeight(),false);

			double psnr = image->PSNR(imgOut);
			cout << "PSNR : " << psnr << endl;
			
			cout << "Entropy : " << imgOut->entropy(0) << endl;
			
			imgOut->saveHistogram(imgOut->histogram(0),"histoOut.dat");
			
			cout << "Saving chifree" << endl;
			imgOut->save(imageOutPath);
		}
		else
		{	
			cout << "Desubstitution with key " << key << endl;
			ImageBase* imgOut = desubtitution(image,key); // new ImageBase(image->getWidth(), image->getHeight(),false);

			double psnr = image->PSNR(imgOut);
			cout << "PSNR : " << psnr << endl;
			
			cout << "Entropy : " << imgOut->entropy(0) << endl;
			
			imgOut->saveHistogram(imgOut->histogram(0),"histoOut.dat");
			
			cout << "Saving dechifree" << endl;
			imgOut->save(imageOutPath);
		}
	}
	else if(action == 2)
	{
		ImageBase* imgBest; double bestEntropy = 1000;  ImageBase* imgOut;
		cout << "CRACKING KEY mwa haha" << endl;
		for(int k = 0; k < 256; k++)
		{
			cout << "Desubstitution with key " << k;
			imgOut = desubtitution(image,k); // new ImageBase(image->getWidth(), image->getHeight(),false);
			
			double entropy = imgOut->entropy(0);
			cout << " | entropy is " << entropy << endl;
			if(entropy < bestEntropy)
			{
				imgBest = imgOut;
				bestEntropy = entropy;
				key = k;
			}
		}
		cout << "Saving cracked image, key was " << key << endl;
		imgBest->save(imageOutPath);
	}
	

	return 0;
}


#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <fstream>
#include <vector>
#include <cstdint>

using namespace std;

ImageBase* readBinary(const char* path, int id)
{
	const int imageWidth = 32;
    	const int imageHeight = 32;
    	const int channels = 3;

    	// Open the binary file for reading
    	std::ifstream file(path, std::ios::binary);

    	if (!file) {
        	std::cerr << "Failed to open the binary file." << std::endl;
        	return NULL;
    	}

	const int imageOffset = id * (1 + imageWidth * imageHeight * channels);

    // Seek to the beginning of the desired image
    	file.seekg(imageOffset);

    	// Read the label and pixel values for the first image
    	uint8_t label;
    	std::vector<uint8_t> pixelData(imageWidth * imageHeight * channels);

    	file.read(reinterpret_cast<char*>(&label), sizeof(label));
    	file.read(reinterpret_cast<char*>(pixelData.data()), pixelData.size());

    	// Create an instance of your custom ImageBase class
    	ImageBase* imgOut = new ImageBase(imageWidth, imageHeight, channels);

    	// Populate the ImageBase instance with pixel values
   	for (int pixelIndex = 0; pixelIndex < imageWidth * imageHeight; ++pixelIndex) {
        	for (int channel = 0; channel < channels; ++channel) {
            	uint8_t pixelValue = pixelData[pixelIndex + channel * imageWidth * imageHeight];
            	imgOut->set(pixelIndex, channel, pixelValue);
        	}
    	}
    	
    	return imgOut;
}

float filterAvg[9] = 
	{1.0/9.0,	1.0/9.0,	1.0/9.0,
	1.0/9.0,	1.0/9.0,	1.0/9.0,
	1.0/9.0,	1.0/9.0,	1.0/9.0};

float filterLaplace[9] = 
	{0,1,0,
	1,-4,1,
	0,1,0};
	
float filterDx[9] = 
	{0,0,0,
	1,-2,1,
	0,0,0};

float filterDy[9] = 
	{0,1,0,
	0,-2,0,
	0,1,0};
	
float filterGauss[9] = 
	{1.0/16.0,	2.0/16.0,	1.0/16.0,
	2.0/16.0,	4.0/16.0,	2.0/16.0,
	1.0/16.0,	2.0/16.0,	1.0/16.0};

 
float activation(float v)
{
	return max(0.f,v);
}

int applyFilter(ImageBase* input, const float* filter, int x, int y,int rgb)
{
	float value = 0;
	for(int dx = 0 ; dx < 3; dx++)
	{
		for(int dy = 0 ; dy < 3; dy++)
		{
			value += input->get(x+dx,y+dy,rgb) * filter[dx + (dy*3)];
		}
	}
	return (int)activation(value);
}

void convoluate(ImageBase* output, ImageBase* input,const float* filter, int rgb)
{	
	for(int x = 0 ; x < input->getWidth() - 2; x++)
	{
		for(int y = 0 ; y < input->getHeight() - 2; y++)
		{
			int value = applyFilter(input,filter,x,y,rgb);
			output->set(x,y,rgb,value);
		}
	}
}

ImageBase* convoluate( ImageBase* input,const float* filter)
{	
	int w = input->getWidth() - 2;
	int h = input->getHeight() - 2;
	
	ImageBase* img = new ImageBase(w,h,1);
	
	convoluate(img,input,filter,0);
	convoluate(img,input,filter,1);
	convoluate(img,input,filter,2);
	
	return img;
}

vector<ImageBase*> generateConvolutions(vector<ImageBase*> inputs)
{
	vector<ImageBase*> convos;
	
	for(ImageBase* input : inputs)
	{
		convos.push_back(convoluate(input,filterAvg));
		convos.push_back(convoluate(input,filterLaplace));
		convos.push_back(convoluate(input,filterDx));
		convos.push_back(convoluate(input,filterDy));
		convos.push_back(convoluate(input,filterGauss));
	}
	
	return convos;
}



ImageBase* poolImage(ImageBase* input)
{		
	ImageBase* pool = new ImageBase(input->getWidth()/2,input->getHeight()/2,1);
	
	for(int x = 0 ; x < pool->getWidth(); x++)
	{
		for(int y = 0 ; y < pool->getHeight(); y++)
		{	
			for(int c = 0 ; c < 3; c++)
			{
				int v = max(input->get(x*2,y*2,c),max(input->get(x*2+1,y*2,c),max(input->get(x*2,y*2+1,c),input->get(x*2+1,y*2+1,c))));
				pool->set(x,y,c,v);
			}	
		}
	}
	
	return pool;
}

vector<ImageBase*> poolConvolutions(vector<ImageBase*> convos)
{
	vector<ImageBase*> pools;
	
	for(ImageBase* conv : convos)
	{	
		pools.push_back(poolImage(conv));
	}
	
	return pools;
}
	
vector<unsigned char> caracteristicVector(vector<ImageBase*> datas)
{
	vector<unsigned char> v;
	
	for(ImageBase* img : datas)
	{	
		for(int i = 0; i < img->getSize();i++)
		{
			for(int c = 0; c < 3; c++)
			{
				v.push_back(img->get(i,c));
			}
		}
	}
	
	return v;
}

ImageBase* caracteristicImage(vector<unsigned char> cv)
{
	int s = cv.size()/3;
	int w = sqrt(s);
	ImageBase* img = new ImageBase(w,w,1);
	
	int j = 0;
	for(int i = 0; i < img->getSize();i++)
	{
		for(int c = 0; c < 3; c++)
		{
			img->set(i,c,cv.at(j));
			j++;
		}
	}
	
	return img;
}

float* ouputLayer(vector<unsigned char> cv)
{
	float* lay = new float[10];

	int ws = 10 * cv.size();
	float* weights = new float[ws];

	for(int i = 0; i < ws; i++)
	{
		weights[i] = -1 + (2 * (rand()/(float)RAND_MAX));
	}

	for(int i = 0; i < 10; i++)
	{
		float val = 0;
		for(int j = 0; j < cv.size(); j++)
		{
			val += (cv.at(j) / 255.f) * weights[j*i];
		}
		val /= cv.size();
		val = activation(val);
		lay[i] = val;
	}

	return lay;
}

int main(int argc, char **argv)
{
	if (argc != 5)
	{
		printf("Usage: Database Path ; Image ID ; Image Visualisation Path ; Debug Image Path \n");
		return 1;
	}
	char databasePath[250] , imageToPPMPath[250], debugPath[250]; int id;
	sscanf (argv[1],"%s",databasePath) ;
	sscanf (argv[2],"%d",&id) ;
	sscanf (argv[3],"%s",imageToPPMPath) ;
	sscanf (argv[4],"%s",debugPath) ;

	srand (69);

	cout << endl;

	cout << "Loading image " << id << "°" << databasePath << endl;
	ImageBase* imgIn = readBinary(databasePath,id); // new ImageBase(image->getWidth(),image->getHeight(),0);
	cout << "Saving image "<< endl;
	imgIn->save(imageToPPMPath);
	
	ImageBase* imgOut = convoluate(imgIn,filterLaplace);
	imgOut->save("lapl.ppm");

	vector<ImageBase*> inputs; inputs.push_back(imgIn);
	
	int size = imgIn->getWidth(); int i = 0;
	while(size > 2)
	{
		vector<ImageBase*> convos = generateConvolutions(inputs);

		vector<ImageBase*> pools = poolConvolutions(convos);
		
		pools[0]->save(&(to_string(i) + string(debugPath))[0]);
		
		size = pools.at(0)->getWidth();
		inputs = pools;
		i++;
	}

	vector<unsigned char> cv = caracteristicVector(inputs);
	ImageBase* ci = caracteristicImage(cv);
	ci->save("cv.ppm");

	float* olayer = ouputLayer(cv);

	cout << "OUTPUT LAYER" << endl;
	for(int i = 0; i < 10; i++)
	{
		cout << "  > " << i << " : " << olayer[i] << "  " << endl;
	}


	return 0;
}


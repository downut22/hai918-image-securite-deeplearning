#include <igl/opengl/glfw/Viewer.h>

#include <igl/readOFF.h>
#include <igl/opengl/glfw/Viewer.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>


using namespace std;

Eigen::MatrixXd V;
Eigen::MatrixXi F;

vector<Eigen::Vector3d> points;

Eigen::Vector3d barycenter;

vector<Eigen::Vector3d> sphericalPoints;
double minNorm,maxNorm;
vector<double> norms;
vector<double> normalizedNorms;

vector<int> histogram;
vector<vector<int>> histogramValueIds;

vector<Eigen::Vector3d> outSphericalPoints;
vector<Eigen::Vector3d> cartesianPoints;

string messageStr = "Hello world";
vector<unsigned char> message;
vector<bool> messageBins;

vector<double> messageNorms;

float alpha;

vector<Eigen::Vector3d> eigenToVector(const Eigen::MatrixXd epoints)
{
	vector<Eigen::Vector3d> vpoints;
	for (int i = 0; i < epoints.rows(); ++i) 
  	{
        Eigen::Vector3d point(epoints(i, 0), epoints(i, 1), epoints(i, 2));
        vpoints.push_back(point);
   	}
   	return vpoints;
}

Eigen::MatrixXd vectorToEigen(const vector<Eigen::Vector3d>& vpoints)
{
    int numPoints = vpoints.size();

    if (numPoints == 0) {
        // Return an empty matrix if the vector is empty
        return Eigen::MatrixXd();
    }

    Eigen::MatrixXd epoints(numPoints, 3);

    for (int i = 0; i < numPoints; ++i) {
        epoints(i, 0) = vpoints[i](0); // x-coordinate
        epoints(i, 1) = vpoints[i](1); // y-coordinate
        epoints(i, 2) = vpoints[i](2); // z-coordinate
    }

    return epoints;
}

Eigen::Vector3d processBarycenter(const std::vector<Eigen::Vector3d>& points) 
{
    Eigen::Vector3d bar = Eigen::Vector3d::Zero();
    for (const Eigen::Vector3d& point : points) 
    {
        bar += point;
    }

    bar /= static_cast<double>(points.size());

    return bar;
}

Eigen::Vector3d cartesianToSpherical(const Eigen::Vector3d& cartesianPoint, const Eigen::Vector3d& bar) 
{
    Eigen::Vector3d sphericalPoint;

    // Calculate r (norm)
    sphericalPoint(0) = (cartesianPoint - bar).norm();

    // Calculate θ (polar angle)
    sphericalPoint(1) = std::acos((cartesianPoint(2) - bar(2)) / sphericalPoint(0));

    // Calculate φ (azimuthal angle)
    sphericalPoint(2) = std::atan2(cartesianPoint(1) - bar(1), cartesianPoint(0) - bar(0));

    return sphericalPoint;
}

Eigen::Vector3d sphericalToCartesian(const Eigen::Vector3d& sphericalPoint, const Eigen::Vector3d& bar) 
{
    Eigen::Vector3d cartesianPoint;

    // Calculate X
    cartesianPoint(0) = sphericalPoint(0) * std::sin(sphericalPoint(1)) * std::cos(sphericalPoint(2)) + bar(0);

    // Calculate Y
    cartesianPoint(1) = sphericalPoint(0) * std::sin(sphericalPoint(1)) * std::sin(sphericalPoint(2)) + bar(1);

    // Calculate Z
    cartesianPoint(2) = sphericalPoint(0) * std::cos(sphericalPoint(1)) + bar(2);

    return cartesianPoint;
}

vector<Eigen::Vector3d> cartesiansToSphericals(const vector<Eigen::Vector3d>& cps, const Eigen::Vector3d& bar)
{
	vector<Eigen::Vector3d> sps;
	for(Eigen::Vector3d p : cps)
	{
		sps.push_back(cartesianToSpherical(p,bar));
	}
	return sps;
}

vector<Eigen::Vector3d>  sphericalsToCartesians(const vector<Eigen::Vector3d>& sps, const Eigen::Vector3d& bar)
{
	vector<Eigen::Vector3d> cps;
	for(Eigen::Vector3d p : sps)
	{
		cps.push_back(sphericalToCartesian(p,bar));
	}
	return cps;
}

vector<double> extractNorms(const std::vector<Eigen::Vector3d>& sps)
{
	vector<double> ns;
	for (const Eigen::Vector3d& sp : sps) 
    {
    	ns.push_back(sp(0));
    }
    return ns;
}

void insertNorms(const vector<double>& inorms, std::vector<Eigen::Vector3d>& sps)
{
	for (int i = 0; i < inorms.size();i++)
    {
    	sps.at(i)(0) = inorms.at(i);
    }
}

void processMinMax(const vector<double>& inorms, double& maxNorm, double& minNorm) 
{
    maxNorm = -std::numeric_limits<double>::infinity();
    minNorm = std::numeric_limits<double>::infinity();

    for (double norm : inorms) 
    {
        if (norm > maxNorm) 
        {
            maxNorm = norm;
        }
        if (norm < minNorm) 
        {
            minNorm = norm;
        }
    }
}

vector<double> normalizeValues(const vector<double>& values, double maxValue, double minValue)
{
	vector<double> nvals;

	for(double val : values)
	{
		double nval = (val - minValue) / (maxValue - minValue);
		nvals.push_back(nval);
	}

	return nvals;
}

vector<double> denormalizeValues(const vector<double>& values, double maxValue, double minValue)
{
	vector<double> nvals;

	for(double val : values)
	{
		double nval = minValue + (val * (maxValue - minValue));
		nvals.push_back(nval);
	}

	return nvals;
}

void saveHistogram(const vector<int>& histo,char* path)
{
	ofstream flux(path); double step = (maxNorm - minNorm) / (float)(histo.size());
	for(int i = 0; i < histo.size();i++)	{	flux << (minNorm + (i*step))  << " " << histo[i] << std::endl;}
}

vector<double> denormalizeHistogram(const vector<double>& inorms)
{
	vector<double> res; res.resize(inorms.size());

	double step = (maxNorm - minNorm) / (float)(histogram.size());
	for(int b = 0; b < histogram.size();b++)
	{
		double bot = minNorm + (step * b);
		double top = minNorm + (step * (b+1));

		for(int id : histogramValueIds.at(b))
		{
			double nval = bot + (inorms.at(id) * (top - bot));
			res.at(id) = nval;
		}
	}

	return res;
}

void normalizeHistogram(vector<double>& inorms)
{
	inorms.resize(norms.size());
	double step = (maxNorm - minNorm) / (float)(histogram.size());
	for(int b = 0; b < histogram.size();b++)
	{
		double bot = minNorm + (step * b);
		double top = minNorm + (step * (b+1));

		for(int id : histogramValueIds.at(b))
		{
			double nval = (norms.at(id) - bot) / (top - bot);
			inorms.at(id) = nval;
		}
	}
}

vector<int> processHistogram(const vector<double>& values, double maxValue, double minValue, int k)
{
	vector<int> h; h.resize(k);
	for(int i = 0; i < k; i++)
	{
	 	h.at(i) = 0; 
	 	vector<int> vd;
	 	histogramValueIds.push_back(vd);
	}

	double step = (maxValue - minValue) / (float)(k);
	int id = 0;
	for(double val : values)
	{
		for(int i = 0; i < k; i++)
		{
			double bot = minValue + (step * i);
			double top = minValue + (step * (i+1));
			if(val > bot && val <= top)
			{
				h.at(i)++; 
				histogramValueIds.at(i).push_back(id);
				break;
			}
		}
		id++;
	}

	return h;
}

vector<unsigned char> stringToVector(const string& input) 
{
    vector<unsigned char> result(input.begin(), input.end());
    return result;
}

string vectorToString(const vector<unsigned char>& input) 
{
    string result(input.begin(), input.end());
    return result;
}

vector<bool> vectorUcToBin(const vector<unsigned char>& input) 
{
	vector<bool> bv;
    
    for (unsigned char byte : input) 
    {
        for (int i = 0; i < 8; ++i) 
        {
            bv.push_back(byte & (1 << i));
        }
    }
    
    return bv;
}

vector<unsigned char> binToVectorUc(const vector<bool>& bv) 
{
    vector<unsigned char> ucv;
    
    for (size_t i = 0; i < bv.size(); i += 8) 
    {
        unsigned char byte = 0;
        for (int j = 0; j < 8; ++j) 
        {
            if (i + j < bv.size() && bv[i + j]) 
            {
                byte |= 1 << j;
            }
        }
        ucv.push_back(byte);
    }
    
    return ucv;
}

double processBinMean(const vector<double>& inorms, const vector<int>& ids)
{
	if(ids.size() <= 0){return 0;}

	double m = 0;
	for(int id : ids)
	{
		m += inorms.at(id);
	}
	m /= ids.size();
	return m;
}

vector<double> processBinMeans(const vector<double>& inorms, const vector<vector<int>>& idvs)
{
	vector<double> ds;

	for(vector<int> ids : idvs)
	{
		double m = processBinMean(inorms,ids);
		ds.push_back(m);
	}

	return ds;
}

void adjustBin(vector<double>& inorms, const int& index, float ki)
{
	for(int id : histogramValueIds.at(index))
	{
		inorms.at(id) = pow(normalizedNorms.at(id),ki);
	}
}

vector<double> insertMessage(vector<bool> m,float power = 0.05f)
{
	vector<double> res; vector<double> ks;
	for(double v : normalizedNorms)
	{
		res.push_back(v);
		ks.push_back(1);
	}

	vector<double> means = processBinMeans(res,histogramValueIds);
	if(means.size() != m.size()){cout << "Error : means size not equal to message size" << endl;}

	vector<int> toProcess;
	for(int i = 0; i < means.size();i++)
	{
		toProcess.push_back(i);	
	}

	while(toProcess.size() > 0)
	{
		//cout << toProcess.size() << "/" << means.size() <<  " left to process" << endl;
		for(int i = 0; i < toProcess.size();i++)
		{
			int id = toProcess.at(i);
			bool v = m.at(id);
			double mean = means.at(id);
			//cout << "    mean " << id << "  = " << mean << (v ? "  upping" : "  downing") << endl;
			bool ok = v ? (mean > (0.5 + alpha)) : (mean < (0.5 - alpha));
			if(!ok)
			{
				//float ki = (mean < (0.5 - alpha)) ? 1.1f : 0.9f;//k : (1.0f / k);//ki is fixed, start at 1
				ks.at(id) = v ? (ks.at(id) - power) : (ks.at(id) + power);

				adjustBin(res,id,ks.at(id));
				means.at(id) = processBinMean(res,histogramValueIds.at(id));
			}
			else
			{
				toProcess.erase(toProcess.begin() + i);
				i--;
			}
		}
	}

	/*for(int i = 0; i < normalizedNorms.size();i++)
	{
		res.at(i) = pow(normalizedNorms.at(i),ks.at(i));
	}*/

	return res;
}

vector<bool> extractMessageFromHistogram()
{
	vector<bool> res;

	vector<double> means = processBinMeans(normalizedNorms,histogramValueIds);
	for(double mean : means)
	{
		bool b = mean > 0.5f;
		res.push_back(b);
	}

	return res;
}

float BER(const vector<bool> mb, const vector<bool> me)
{
	int s = 0;
	for(int i = 0; i < mb.size();i++)
	{
		s += (mb.at(i) != me.at(i) ? 1 : 0);
	}

	return s / (float)mb.size();
}

float RMSE(const vector<Eigen::Vector3d> basePoints, vector<Eigen::Vector3d> messagePoints)
{
	float se = 0;

	for(int i = 0; i < basePoints.size(); i++)
	{
		Eigen::Vector3d diff = basePoints.at(i) - messagePoints.at(i);
		se += diff.dot(diff);
	}

	return sqrt(se / basePoints.size());
}

void displayMessageBin(const vector<bool> mbins)
{
	cout << " >> ";
	for(bool b : mbins)
	{
		cout << (b ? "1" : "0") << " ";
	}
	cout << endl;
}

int main(int argc, char *argv[])
{
	if(argc < 3)
	{
		cout << "Usage : ./tp_bin model_path alpha (kbin)" << endl;
		return 0;
	}

	bool extract = argc >= 4;

	char modelPath[250]; int kbin;
	sscanf (argv[1],"%s",modelPath) ;
	sscanf (argv[2],"%f",&alpha);
	if(extract) { sscanf (argv[3],"%d",&kbin) ; }

	string modelPathStr = string(modelPath);
	modelPathStr = "../" + modelPathStr;

	cout << "Loading model " << modelPathStr << endl;
  // Load a mesh in OFF format
  	igl::readOFF(modelPathStr.c_str(), V, F);

  	points = eigenToVector(V);

  	barycenter = processBarycenter(points);
  	cout << endl << "Barycenter : " << barycenter.transpose() << endl;

  	sphericalPoints = cartesiansToSphericals(points,barycenter);

  	norms = extractNorms(sphericalPoints);

  	processMinMax(norms,maxNorm,minNorm);
	cout << endl << "Min : " << minNorm << "    max : " << maxNorm << endl;

	if(extract)
	{
		cout << "Making " << kbin << " bins histogram" << endl;
		histogram = processHistogram(norms,maxNorm,minNorm,kbin);
		cout << endl << "Saving histogram" << endl;
		saveHistogram(histogram,"../histo_ex.dat");
		cout << "Normalizing histogram" << endl;
		normalizeHistogram(normalizedNorms);

		messageBins = extractMessageFromHistogram();
		cout << "Extracted " << messageBins.size() << " values" << endl;
		message = binToVectorUc(messageBins);
		string extractedMessage = vectorToString(message);

		vector<bool> oribins = vectorUcToBin(stringToVector(messageStr));
		cout<<endl<<"Original message bins : " << endl;
		displayMessageBin(oribins);
		cout<<"Extracted message bins : " << endl;
		displayMessageBin(messageBins);

		cout << endl << "BER : " << BER(oribins,messageBins) << endl;

		cout << "Message : " << extractedMessage << endl;
	}
	else
	{
		message = stringToVector(messageStr);
		messageBins = vectorUcToBin(message);

		cout << "Making " << messageBins.size() << " bins histogram" << endl;
		histogram = processHistogram(norms,maxNorm,minNorm,messageBins.size());

		cout << endl << "Saving histogram" << endl;
		saveHistogram(histogram,"../histo.dat");

		cout << "Normalizing histogram" << endl;
		normalizeHistogram(normalizedNorms);

		cout << endl << "Inserting message " << messageBins.size() << " with alpha = " << alpha << endl;
		messageNorms = insertMessage(messageBins);

		string messageRecon = vectorToString(binToVectorUc(messageBins));

		cout << messageStr << "   " << messageRecon << endl;

		norms = denormalizeHistogram(messageNorms);//denormalizeValues(messageNorms,maxNorm,minNorm);

		insertNorms(norms,sphericalPoints);

		vector<Eigen::Vector3d> outCps = sphericalsToCartesians(sphericalPoints,barycenter);

		cout << endl << "RMSE : " << RMSE(points,outCps) << endl;

		V = vectorToEigen(outCps);

		igl::writeOFF("tatooed.off",V,F);
	}

  // Plot the mesh
  	igl::opengl::glfw::Viewer viewer;
  	viewer.data().set_mesh(V, F);


  	

	viewer.launch();

  	return 0;
}

/*int main(int argc, char *argv[])
{
  // Inline mesh of a cube
  const Eigen::MatrixXd V= (Eigen::MatrixXd(8,3)<<
    0.0,0.0,0.0,
    0.0,0.0,1.0,
    0.0,1.0,0.0,
    0.0,1.0,1.0,
    1.0,0.0,0.0,
    1.0,0.0,1.0,
    1.0,1.0,0.0,
    1.0,1.0,1.0).finished();
  const Eigen::MatrixXi F = (Eigen::MatrixXi(12,3)<<
    1,7,5,
    1,3,7,
    1,4,3,
    1,2,4,
    3,8,7,
    3,4,8,
    5,7,8,
    5,8,6,
    1,5,6,
    1,6,2,
    2,6,8,
    2,8,4).finished().array()-1;

  // Plot the mesh
  igl::opengl::glfw::Viewer viewer;
  viewer.data().set_mesh(V, F);
  viewer.data().set_face_based(true);
  viewer.launch();
}*/
